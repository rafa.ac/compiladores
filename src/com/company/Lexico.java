package com.company;

import java.io.*;
import java.util.ArrayList;

public class Lexico {
    public ArrayList<Token> analisa(String arquivo) {
        ArrayList<Token> list = new ArrayList<>();
        int ch;

        try {
            PushbackReader reader = new PushbackReader(new BufferedReader(new InputStreamReader(new FileInputStream(arquivo), "US-ASCII")));

            while ((ch = reader.read()) != -1) {
                System.out.println((char) ch);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}
